#!/bin/sh
set -e
sh -c "printf line%s $'\n'" "/bin/sh" | tee /dev/stdout > line
read -r c rest <<EOF
$(wc -c line)
EOF
test "$c" -eq 5
